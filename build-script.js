const fs = require('fs-extra');
const concat = require('concat');

(async function build() {

  const files =[
    './dist/elements/runtime.js',
    './dist/elements/polyfills.js',
    './dist/elements/main.js'
  ];

  await fs.ensureDir('elements');
  await concat(files, 'elements/exo-custom-element.js');
  await fs.copyFile('./dist/elements/styles.css', 'elements/styles.css');
  await fs.copy('./dist/elements/assets/', 'elements/assets/');
  console.info('Exo elements created successfully!');

})();
